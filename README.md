# deg.form.gb

Some components for displaying data of lists and results of database-queries. These are DegComboBox(stable) and DegTableView(stable). They work fine with Gambas Postgresql and Mysql database connections. Other databases and ODBC connections are not tested and not prooved to work.

The goal of these controls is to provide advanced unbound controls for displaying and editing data arrays or Gambas results (a special Gambas object that contains data of a db-query).

"Unbound" means, that visible data is not tied directly to database tables and no connection must be held open as long as the data is edited by the user. This is especially useful for database frontend software to avoid locking problems when a lot of users query and update data on the same tables. As a programmer using Deg-controls you can query the data from the database and then display via the control and let the user work with the data. As soon as the user updated anything, the Deg-control sends events "BeforeUpdate", "AfterUpdate" ... so that you can examine, verify and/or send the changed data back to the database.

As in relational databases tables usually are joined via foreign keys, DegComboBox can read and save an index but display and let search data from another column of the same table.

DegTableView containes a method to implement DegComboBox so that the programmer can display joined data from other tables. The foreign key is stored in one table (the one DegTableView is based on), but the joined data from another can be displayed and choosen via DegComboBox.

These components are inspired from MS Access and developed in Gambas because Gambas can be a very cool and nice performing database frontend.

# DegComboBox: Improved ComboBox for Gambas

![Degcombobox](degcombobox.png)

This is a combobox which can be feeded with arrays like ["one", "two", "three"] or [[1,"one"], [2,"two"], [3,"three"]] or a Gambas [Result](http://gambaswiki.org/wiki/comp/gb.db/result) (that is the special Gambas array, that contains the result of a database query).

The combobox provides the properties "Text" and "Index". So for instance you can use it to display or change data based on joined tables via primary keys.

## EXAMPLES

### Fill with result of a database query

    Dim cbox as new DegComboBox
    'Connection not really complete here / just an example
    Dim con as new Connection
    Dim res as Result
    Dim qry as string
    
    qry = "select pkprodukt, produktname from tbprodukt order by produktname;"
    res = con.Exec(qry)
    
    cbox.DefineColumnWidths([20, 120])
    cbox.ColumnBound = 0
    cbox.ColumnSearch = 1
    cbox.SetListData(res)
    cbox.show

### Fill with columns and rows defined in an array

    Dim cbox as new DegComboBox
    dim clist as variant[]
    clist = [[1, "Anis"], [2, "Buxta"], [3, "Crown"], [4, "Donner"]]
    
    ' Content of column 0 can be read in cbox.Index
    cbox.ColumnBound = 0
    
    ' In column 1 is the text searched, it will be in cbox.Text after save
    cbox.ColumnSearch = 1
    
    ' Look here: The Index column is hidden
    cbox.DefineColumnWidths([0, 120])
    cbox.SetListData(clist)
    cbox.show

### Fill with a simple 1-column array

    Dim cbox as new DegComboBox
    dim clist as variant[]
    
    clist = ["Anis", "Buxta", "Crown", "Danke", "Donner"]
    clist.Insert(["Freilicht", "Griebenschmalz", "Milben", "Mineralfutter"])
    cbox.SetListData(clist)
    cbox.show
    
***

The Combobox first raises an Event "BeforeUpdate"

    Public Sub cbox_BeforeUpdate(Text as String, Index as Variant)

        Debug Index & " : " & Text

    End

which can be canceled via **Stop Event** and after that it raises "AfterUpdate".

***
## Autocompletion

If the box is filled with a list like above, the user types "an" 
and if there is a corresponding item in the list, the combobox opens the grid.

"Anis" is shown, the part of the user's input is unselected, the additional 
part of the word, which is found by the combobox is selected.

By using Arrow-up/down or Pgup/down the user can scroll through the 
list's data, select an item with &lt;Enter&gt; or &lt;Return&gt;.

# DegTableView: TableView with builtin DegCombobox

![Degcombobox](degtableview.png)

## EXAMPLES

A simple example is the form "FmTableWithCbo" in the "Test" folder of the project. Here you can see how DegComboBox and DegTableView work together. This example offers a DegTableView with two DegComboBoxes, the first of them presenting data based on an index. The second one is used to make a suggestion to the user but let him freely enter other data.

***

	Private Sub FillTable()

        '"degtab" is the table on the form
    
        Dim tlist As Variant[]
        Dim clist As Variant[]
        Dim qry As String
        Dim res As Result
    
        Dim i As Integer
    
        'DegTableview Data
        tlist = [[1, "Albert", Date()], [2, "Berta", Date()], [3, "Cäsar", Null], [4, "Paula", ""], [3, "Albert", Date()]]
    
        'DegComboBoxes Data
        clist = [[1, "Eins"], [2, "Zwei"], [3, "Drei"], [4, "Vier"]]
        dlist = ["Albert", "Berta", "Cäsar", "Paula"]
    
        With degtab
    
            .SetColumn("number", "Number", 150)
            .SetColumn("name", "Name", 200)
            .SetColumn("datum", "Datum", 100, "d.m.yyyy")
            .SetTableData(tlist)
    
    		.ComboButtons = True ' has to be before SetComboBox
            
            ' this DegComboBox is based on the index of tlist[x][0]
            ' but will show the content of clist[x][1] in the DegTableView:
            .SetComboBox(0, clist)
            
            ' this one allows any content entered and has only one column
            ' as you see in dlist
            .SetComboBox(1, dlist, False)
            .Readonly = False
            .AllowInsert = True
            .AllowDelete = True
            .Show
        End With
    
	End

***


