#DegColumn
_GridView_Column
C
'Definition of a column in DegTableView. If changed,
'DegTableView.Show() has to be executed to apply changes.
Format
p
s

Name
p
s

'
Text
p
s

'Text in header of column
Title
p
s

'Text in header of column
Alignment
p
i

ShowAs
p
i

Type
p
i

Width
p
i

'
W
p
i

'
ComboData
p
Variant[]

Readonly
p
b

ComboBox
p
DegComboBox

_new
m


#DegComboBox
ButtonBox
C
'ComboBox like known from MS Access.
'Can be feeded with a two-dimensional array of Variants or a Gambas Result or
'a special object called DegOrm
_Group
C
s
"Deg"
_Family
C
s
"Form"
_DrawWith
C
s
"ComboBox"
_Properties
C
s
"*, Index, Text, ColumnBound, ColumnSearch, ListItemsOnly, -ReadOnly"
_DefaultSize
C
s
"28,4"
_DefaultEvent
C
s
"Click"
ListItemsOnly
p
b

'Default true. If true then ComboBox allows only listed items.
_Embedded
p
b

'Set by DegTableView when embedded
Index
p
v

'Contains value of bound column after commit. After write
'Cbobox is displaying corresponding data.
Text
p
s

'Contains data of column wherein is searched.
'After delete or set to Null also Index is deleted.
ColumnBound
p
i

'ColumnBound (column that holds the "real" data).
'Column position (starting from 0).
ColumnSearch
p
i

'ColumnSearch (column that holds the data which is showed
'and wherein is searched). Default 0.
Changed
p
b

'Returns or sets weather Combobox.Text has changed
Rowcount
p
i

'How much rows are shown. Defines the height of the element when the combobox is opened.
GridData
r
v

'GridData of Combobox
:BeforeUpdate
:

(Text)s(Index)v
'Event BeforeUpdate (Test, Index) can be canceled with "Stop Event"
:AfterUpdate
:

(Text)s(Index)v
'Event AfterUpdate (Text, Index)
:Save
:

(Text)s(Index)v
'Synonym for Event AfterUpdate(Text As String, Index As Variant)
:NotInList
:

(Text)s
'If ListItemsOnly = True, this event presents the data which was entered
'but does not match. A Stop Event in the event handler of NotInList
'does not set Text to Null.
_new
m


Show
m


Open
m


'Opens the combobox, aka shows the boxes' window
_InputBox_Change
m


_InputBox_GotFocus
m


_InputBox_DblClick
m


_InputBox_Click
m


'Opens the ComboBox or (should) close it, when open.
_Window_Close
m


_Window_Move
m


_Window_Resize
m


Gridwin_Deactivate
m


DefineColumnWidths
m

(Widths)Integer[];
'Synonym for SetColumnWidths(Widths)
SetColumnWidths
m

(Widths)Integer[];
'Widths = array of widths (in pixel units) of the columns in right order.
'To hide first column with a primarykey, set it to 0.
SetListData
m

(Data)v
'Set data of DegComboBox. Can be either a twodimensional array of Variant or
'a Gambas Result (gb.db) or DegOrm (curently not free code, sorry)
_InputBox_KeyPress
m


GridV_KeyPress
m


GridV_Click
m


ColumnContent
m
v
(Col)i[(Row)v]
'Returns the content of column Col of DegComboBox. If Row is Null
'and the currend row is definded by Index that is choosen, else row 0.
'
'As DegComboBox can contain a lot of columns, this function allows to
'pick the value of each.
GetValueByIndex
m
v
(Index)v[(DataColumn)i]
'Returns any value of DegComboBox for a given Index. DataColum
'defines the column where the data resides. If it is Null, then
'GetValueByIndex returns the data of the column defined in ColumnSearch.
#DegTableView
TableView
C
'Deganius DegTableView, delivers a Gambas TableView
'based on data of an array or db-result or a class DegOrm.
'
'Can display a DegComboBox inside and bind it's index to a column.
AsString
C
i
0
AsCheckbox
C
i
1
AsRichText
C
i
2
_Group
C
s
"Deg"
_Family
C
s
"Form"
_DrawWith
C
s
"TableView"
_Properties
C
s
"*, ComboButtons, Readonly, AllowDelete, AllowInsert, AllowCopyPaste"
RowChanged
p
i

'Number of row that has been changed. If nothing changed, it is -1.
Readonly
p
b

'Default true. If set to false, DegTableView data ist changeable.
AllowDelete
p
b

'Default false.
AllowInsert
p
b

'Default false
AllowCopyPaste
p
b

'Allow Ctrl-C Ctrl-V? Default false.
ColumnDefs
r
DegColumn[]

'Array of all column definitions (DegColumn)
Layout
r
s

:BeforeCellSave
:

(Row)i(Column)i(ColumnName)s(OldValue)v(NewValue)v
'To inspect user input. If cancel is returned then old data is restored.
:AfterCellSave
:

(Row)i(Column)i(ColumnName)s(Value)v
'For actions (ie jump to another column etc.) after cell data has been entered and is ok.
:BeforeRowUpdate
:

(Row)i(OldData)Collection;(NewData)Collection;
:AfterRowUpdate
:

(Row)i(NewData)Collection;
:BeforeRowInsert
:

(Row)i(NewData)Collection;
:AfterRowInsert
:

(Row)i(NewData)Collection;
:BeforeRowDelete
:

(Row)i(OldData)Collection;
:AfterRowDelete
:


:ComboBoxNotInList
:

(Row)i(Column)i(Text)s
'If a column contains a DegComboBox with ListItemsOnly set true and the box
'raises NotInList this event is risen and can be canceled via Stop Event.
ComboButtons
p
b

'If set to true, then DegComboBoxes carry buttons.
'Has to be set before SetComboBox.
DBResult
r
Result

'If tabledata was from result, here it is stored.
'But it is only readable, if DegTableView itself is Readonly.
GridData
r
Variant[]

'This two-dimensional array contains the data of DegTableView.
'First dimension contains the rows, second dimension
'contains the columns data. If manipulated,
'DegTableView shows the altered data immediately. Use it like that:
'
'Dim TvData as Variant[]
'TvData = DegTableView.GridData
_new
m


Table_Click
m


Edit
m

[(List)String[];(Readonly)b]
'Edit cell content. List and ReadOnly are for compatibility reasons only and without effect.
Find
m

(Search)s(Column)i[(StartWithRow)i]
'Find first row from top where text contains search and select it
FindNext
m


'Finds next position of the string which was searched with Find
GetComboItem
m
v
(Column)i(BoxColumn)i(Index)v
'Assume DegTableview has DegCombobox defined in [Column]. Searches through
'Combobox.ColumnBound until value is found. Then this function returns the content
'of BoxColumn.
'
'So you can fill DegCombobox with a lot of columns and get all values easily by searching the index.
Table_KeyPress
m


JumpToPos1
m


'If first column(s) are hidden (width 0) sets the column to the first visible.
Table_KeyRelease
m


GetRowData
m
Collection
[(Row)i(Data)Variant[];]
Table_RowClick
m

(Row)i
Table_ColumnClick
m

(Column)i
Table_Insert
m


Table_LostFocus
m


LayoutStore
m


Table_Data
m

(Row)i(Column)i
Table_Change
m


Clear
m


'Clear data, reset changes, but keep columns and definitions.
SetTableData
m

(Data)v[(OnlyColumnNames)b]
'Sets data of DegTableView. Data can be an array of rows (array of columns)
'or a gb.db.Result or a special class named DegOrm.
'
'If data is a gb.db.Result and OnlyColumnNames = True then the
'data of the Tableview takes only columns of the result with an
'identical name like the columns defined in SetColumn
'
'DegOrm is a Model-Class whose properties represent fields of a DB-table,
'as it may contain more fields than to be displayed, columns have to be
'defined before SetTableData with SetColumn.
Reset
m


'Reset all data and columns, so that it can be
'defined and filled new.
Table_Save
m

(Row)i(Column)i(Value)s
'This event occurs when a normal table cell altered and has to be saved,
'if the table cell editor is a DegComboBox then Combobox_save is risen
SetCellData
m
b
(Row)i(Column)i(Value)v
'To insert Data in a cell.
'If another row has been changed but not saved, it will do nothing and
'instead return false.
'
'Use NoTriggerChange = True if you do not want to trigger BeforeCellSave.
'So it can be used to alter the data shown, without alter the data source.
SetComboBox
m

(Col)i(List)v[(ListItemsOnly)b(Layout)Integer[];(ColumnSearch)v]
'Creates DegComboBox in column Col with data List.
'ListItemsOnly defaults to true, Layout defines the
'size of columns.
'
'If data represents one column then ColumnSearch and ColumnBound
'of DegComboBox are 0.
'
'If data represents more than one column, then
'ColumnBound is set to 0 by default And ColumnSearch is set to 1 by default.
'This behaviour makes it easy to integrate tables with foreign keys.
'
'Optional ColumnSearch can be set to another column
SetLayoutStorage
m

(Name)s(Setting)Settings;
'If set, the table will store its layout in Setting under Name/Layout It will
'load its layout from there if it is created the next time.
Show
m


SetLayout
m

(Layout)s
'Set Layout with a Json string
SetColumn
m

(Name)s[(Caption)s(Width)i(Format)s(Alignment)i(type)i(ShowAs)i(Readonly)b]
'Define a column. Set Width to 0 to hide, Format can be set to format shown data.
'Showas can be used to define a view (currently string and checkbox)
ComboBox_NotInList
m

(Text)s
ComboBox_Save
m

(Text)s(Index)v
'This event occurs, after content of Combobox has changed
SetUnChanged
m


'SetUnChanged() if data is altered, but no
'events shall be risen.
LayoutRestore
m


'Restore default Layout
Table_ColumnReSize
m

(Col)i
